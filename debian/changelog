abinit (8.8.4-1) unstable; urgency=medium

  [ Andrius Merkys ]
  * New upstream version (Closes: #907395)
  * Updating patch enable_pstricks_with_pdflatex.patch.
  * Removing upstream-integrated patch gcc7_random_seek.patch.
  * Removing patch help_index.patch as HTML documentation is no longer present
    in the upstream tarball.
  * Bumping compat.
  * debian/rules: autotools-dev sequence has been replaced by dh in debhelper.
  * Dropping build-dependency on autotools-dev (included in debhelper).
  * Using upstream-preferred method instead of dh_autoreconf.
  * Adding one more patch to remove non-existent response.pdf.
  * debian/abinit-doc.install: locations of many docs have been changed.
  * Generating manpages using help2man, as old manpages are gone.
  * Adding help2man to Build-Depends.
  * Setting $HOME to /nonexistent to prevent writing to it.
  * Passing '-g' flag to Fortran compiler.
  * Adding '-ffree-line-length-none' to FCFLAGS, as suggested in
    https://github.com/abinit/abinit/issues/1.
  * debian/copyright: HTTP -> HTTPS.
  * debian/rules: fixing documentation path, skipping compression of .md files.
  * debian/rules: fixing replacement of pseudopotential directory.
  * Adding lintian override to ignore missing source of unused JS file.
  * debian/rules: separating -arch and -indep targets.

  [ Michael Banck ]
  * debian/watch: Updated.

 -- Andrius Merkys <andrius.merkys@gmail.com>  Wed, 29 Aug 2018 08:54:32 -0400

abinit (8.0.8-4) unstable; urgency=medium

  * debian/patches/gcc7_random_seek.patch: Fix typo.

 -- Michael Banck <mbanck@debian.org>  Sat, 05 Aug 2017 15:25:48 -0400

abinit (8.0.8-3) unstable; urgency=medium

  * debian/patches/series: Updated.

 -- Michael Banck <mbanck@debian.org>  Sat, 05 Aug 2017 14:05:18 -0400

abinit (8.0.8-2) unstable; urgency=medium

  * debian/patches/gcc7_random_seek.patch: New patch, fix FTBFS error with GCC7
    (Closes: #853297).

 -- Michael Banck <mbanck@debian.org>  Sat, 05 Aug 2017 12:33:21 -0400

abinit (8.0.8-1) unstable; urgency=medium

  * New upstream version
  * Add texlive-luatex to Build-Depends
    Closes: #835781
  * Remove "Section: doc" from abinit-data package
    Closes: #762914
  * cme fix dpkg-control
  * fix DEP5 names
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Fri, 16 Sep 2016 08:50:14 +0200

abinit (7.8.2-3) unstable; urgency=medium

  [ Michael Banck ]
  * debian/control (Vcs-Browser, Vcs-Svn): Fixed URLs.
  * debian/rules (override_dh_auto_build): New target, builds documentation
    and additional PDFs besides the regular build.
  * debian/patches/enable_pstricks_with_pdflatex.patch: Updated to include
    auto-pst-pdf package, which is required but does not get included
    automatically anymore (Closes: #800736).
  * debian/rules (override_dh_install): Renamed to ...
  * debian/rules (override_dh_auto_install): ... this.  Run dh_auto_install
    once for the regular build and the documentation.

  [ Daniel Leidert ]
  * debian/watch: Fixed.

 -- Michael Banck <mbanck@debian.org>  Sun, 22 Nov 2015 16:42:58 +0100

abinit (7.8.2-2) unstable; urgency=medium

  * Fix copyright of doc/versioning/leaflet.cls
    Closes: #747204

 -- Andreas Tille <tille@debian.org>  Wed, 03 Sep 2014 09:03:48 +0200

abinit (7.8.2-1) unstable; urgency=medium

  * New upstream version (adapted patches)
    Closes: #747259
  * No need to remove documentation without source any more
    since it was removed by upstream
  * moved debian/upstream to debian/upstream/metadata
  * Upstream changed the way to install docs so several changes in
    d/rules and d/abinit-doc.install were needed.  Docs are now
    rebuilded from source
  * Several patches needed to enable building latex source

 -- Andreas Tille <tille@debian.org>  Thu, 21 Aug 2014 11:33:37 +0200

abinit (7.6.3+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version
    Closes: #530036, #732520
  * debian/copyright:
     - reviewed
     - DEP5
     - Files-Excluded: doc/developers/strain_nonlocal/strainpert.pdf
       since this is the only remaining binary without source
  * debian/rules: remove orig-tarball target since with new uscan
    the remaining to be deleted file can be dealt very easily
  * debian/control:
     - Build-Depends: python (to enable running tests)
  * separate packages for docs and examples
  * debian/tests: currently autopkgtest is disabled since it seems
    that abinit needs the full source directory.  Leave some skeleton
    what might be needed in debian/tests anyway.

  [ Michael Banck ]
  * debian/rules: Fix nocheck case in DEB_BUILD_OPTIONS for dh_auto_test. 
  * debian/patches/help_index.patch: New patch, adding an top-level index page
    for the HTML documentation.
  * debian/rules (override_dh_compress): New rule, do not compress .pdf files.
  * debian/patches/testsuite_parallel.patch: New patch, limit number of cores
    for test suite to 2, instead of 6.
  * debian/abinit-doc.install: Install documentation into html subdirectory.
  * debian/rules (override_dh_compress): Do not compress example .in files.
  * debian/control (abinit-examples): Renamed to ...
  * debian/control (abinit-data): ... this.  Also, update descriptions
    accordingly and make abinit Recommend abinit-data.
  * debian/abinit-data.install: New file, install pseudopotentials in
    /usr/share/abinit/psp and tutorials in /usr/share/doc/abinit/examples.
  * debian/rules (DATPKGNAME): New variable.
  * debian/rules (override_dh_install): Modify example .files to take
    pseudopotentials from /usr/share/abinit/psp.  Also, remove HGH subdirectory
    as the pseudopotentials in there get installed in the main directory.
  * debian/TODO: New file.
  * debian/rules (override_dh_auto_test): Run "fast" subset of testsuite by
    default, not full testsuite.
  * debian/rules (override_dh_auto_clean): New target, cleans up upstream test/
    directory after testsuite run.
  * debian/control (Uploaders): Added myself.

 -- Michael Banck <mbanck@debian.org>  Sun, 20 Apr 2014 13:29:58 +0200

abinit (5.3.4.dfsg-4) unstable; urgency=medium

  * Inject abinit into DebiChem SVN since pkg-scicomp was droped
  * debian/control:
     - cme fix dpkg-control
     - debhelper 9
     - slightly enhanced short descriptions to get rid of lintian
       warnings
     - Removed Christophe Prud'homme and Ondrej Certik from Uploaders
       since both seem to be inactive
       Closes: #550410
  * debian/source/format: 3.0 (quilt)
  * debian/rules + debhelper files: Use short dh
  * use autotools-dev
    Closes: #545054

  [ Daniel Leidert ]
  * debian/watch: Fixed.

 -- Andreas Tille <tille@debian.org>  Wed, 08 Jan 2014 14:51:49 +0100

abinit (5.3.4.dfsg-3) unstable; urgency=low

  * Add XS-DM-Upload-Allowed: yes
  * shifted Build-Depends to use the new gfortran based Lapack and Blas
    packages in order to phase out packages dependent on the old g2c/g77
    (Closes: #463942)
  * Homepage field converted to the new style
  * Standards version updated to 3.7.3
  * Old FSF address fixed in debian/copyright
  * abinit-doc moved to the section doc.
  * doc/Makefile.in patch moved to debian/patches managed by quilt 

 -- Ondrej Certik <ondrej@certik.cz>  Tue, 05 Feb 2008 14:09:20 +0100

abinit (5.3.4.dfsg-2) unstable; urgency=low

  [Ondrej Certik]
  * Renamed /usr/bin/newsp to /usr/bin/newsp.abinit. Bug fix: "file
    conflicts between packages", thanks to Michael Ablassmeier (Closes:
    #442237).

 -- Christophe Prud'homme <prudhomm@debian.org>  Fri, 14 Sep 2007 13:05:00 +0200

abinit (5.3.4.dfsg-1) unstable; urgency=low

  [Christophe Prud'homme]
  * Uploaded abinit to main (Closes: #434912)
  * debian/control: updated Maintainers and added Uploaders section
  * debian/rules: enable optimization flags (-O2)

  [Ondrej Certik]
  * Removed non-free files and unnecessary libraries from the upstream tarball
    (see debian/copyright for more details)

 -- Christophe Prud'homme <prudhomm@debian.org>  Sat, 28 Jul 2007 15:04:18 +0200

abinit (5.3.4-0oc1) unstable; urgency=low

  * Unofficial release, packages work, but are not lintian clean yet and
    other small issues are still there.
  * Initial release (Closes: #434912)

 -- Ondrej Certik <ondrej@certik.cz>  Fri, 27 Jul 2007 18:53:34 +0200

